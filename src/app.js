const result = document.getElementById('output');
const button = document.getElementById('btn');

const generatePass = () => {
  let password = '';

  for (let i = 0; i < 4; i++) {
    password += Math.floor(Math.random() * 9 + 1);
  }

  result.innerHTML = password;
};

button.addEventListener('click', generatePass);
